<?php
/*
    Plugin Name: Wordpress Helpers
    Version: 1.0.0
    Author: Alan Chen
    License: GPLv2 or later
    License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

defined('ABSPATH') or die('No script kiddies please!');

define('G_WP_HELPER_VERSION', '1.0.0');
define('G_WP_HELPER_FILE', __FILE__);
define('G_WP_HELPER_PATH', dirname(__FILE__));
define('G_WP_HELPER_URL', plugins_url('', __FILE__));

require_once G_WP_HELPER_PATH . '/vendor/autoload.php';
// Gummiforweb\WpHelpers\WpMenu::init();
Gummiforweb\WpHelpers\WpQuery\QueryHelper::initHooks();
