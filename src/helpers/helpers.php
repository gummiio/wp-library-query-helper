<?php

function query_helper($_ = false) {
    $query = new Gummiforweb\WpHelpers\WpQuery\QueryHelper;

    if (count(func_get_args()) == 1 && func_get_arg(0) === false) {
        return $query;
    }

    if (! func_get_args() || (count(func_get_args()) == 1 && func_get_arg(0) === true)) {
        return $query->getGlobal();
    }

    return call_user_func_array([$query, 'query'], func_get_args())->cache();
}

function last_query_helper() {
    return (new Gummiforweb\WpHelpers\WpQuery\QueryHelper)->last();
}
