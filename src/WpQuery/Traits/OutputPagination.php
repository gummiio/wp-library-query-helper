<?php

namespace Gummiforweb\WpHelpers\WpQuery\Traits;

trait OutputPagination
{
    public function currentPage()
    {
        return get_query_var('paged', 1);
    }

    public function totalPage()
    {
        return $this->getWpQuery()->max_num_pages;
    }

    public function prevPageUrl()
    {
        if ($this->currentPage() == 1) return '';

        return $this->preparePageUrl(get_pagenum_link($this->currentPage() - 1));
    }

    public function prevPage($label = 'Previous Page')
    {
        if (! $this->prevPageUrl()) return '';

        return sprintf(
            '<a href="%s" class="prev-page-link">%s</a>',
            $this->prevPageUrl(),
            $label
        );
    }

    public function nextPageUrl()
    {
        if ($this->currentPage() == $this->totalPage()) return '';

        return $this->preparePageUrl(get_pagenum_link($this->currentPage() + 1));
    }

    public function nextPage($label = 'Next Page')
    {
        if (! $this->nextPageUrl()) return '';

        return sprintf(
            '<a href="%s" class="next-page-link">%s</a>',
            $this->nextPageUrl(),
            $label
        );
    }

    public function pageNumbers($listClass = '')
    {
        if ($this->totalPage() <= 1) return '';

        return sprintf(
            '<ul class="pagination-list %s">%s</ul>',
            esc_attr($listClass),
            $this->getPaginationNumbers()
        );
    }

    public function pagination($listClass = '', $prevLabel = 'Previous Page', $nextLabel = 'Next Page', $args = [])
    {
        if ($this->totalPage() <= 1) return '';

        return sprintf(
            '<ul class="pagination-list %s">%s</ul>',
            esc_attr($listClass),
            $this->getPaginationNumbers(wp_parse_args($args, [
                'prev_next' => true,
                'prev_text' => $prevLabel,
                'next_text' => $nextLabel,
            ]))
        );
    }

    protected function preparePageUrl($url)
    {
        return add_query_arg($this->getCurrentQueryArgs(), $url);
    }

    protected function getCurrentQueryArgs()
    {
        $queryArgs = $_GET;

        if (isset($queryArgs['paged'])) unset($queryArgs['paged']);

        return $queryArgs;
    }

    protected function getPaginationNumbers($args = [])
    {
        $lists = paginate_links(wp_parse_args($args, [
            'prev_next' => false,
            'mid_size' => 3,
            'total' => $this->totalPage(),
            'type' => 'array',
            // 'add_args' => $this->getCurrentQueryArgs()
        ]));

        $output = [];

        foreach ($lists as $list) {
            $parsed = $this->parsePaginationLink($list);

            $output[] = sprintf(
                '<li class="%1$s"><%2$s %3$s class="%4$s">%5$s</%2$s></li>',
                $this->getPaginationItemClass($parsed),
                $parsed['href']? 'a' : 'span',
                $parsed['href']? "href=\"{$parsed['href']}\"" : '',
                implode(' ', $parsed['classes']),
                $parsed['label']
            );
        }

        return implode("\n", $output);
    }

    protected function parsePaginationLink($list)
    {
        $parsed = [
            'classes' => [],
            'href' => '',
            'label' => strip_tags($list)
        ];

        $replace = [
            'prev page-numbers' => 'pagination-link pagination-prev',
            'next page-numbers' => 'pagination-link pagination-next',
            'page-numbers'      => 'pagination-link pagination-number',
            'dots'              => 'pagination-dots',
            'current'           => 'pagination-current',
        ];

        if (preg_match('#class=["|\']([^"\']*)["|\']#', $list, $classes)) {
            $parsed['classes'] = array_merge($parsed['classes'], explode(' ', str_replace(
                array_keys($replace),
                array_values($replace),
                $classes[1]
            )));
        }

        if (preg_match('#href=["|\']([^"\']*)["|\']#', $list, $hrefs)) {
            $parsed['href'] = $hrefs[1];
        }

        return $parsed;
    }

    protected function getPaginationItemClass($parsed)
    {
        if (in_array('pagination-prev', $parsed['classes'])) return 'pagination-item pagination-prev-item';
        if (in_array('pagination-next', $parsed['classes'])) return 'pagination-item pagination-next-item';

        return 'pagination-item';
    }
}
