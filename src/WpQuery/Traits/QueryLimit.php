<?php

namespace Gummiforweb\WpHelpers\WpQuery\Traits;

trait QueryLimit
{
    public static function hookQueryLimit()
    {
        add_action('pre_get_posts', [static::class, 'queryPostTypePostsPerPage']);
        add_action('pre_get_posts', [static::class, 'queryTaxonomyPerPage']);

        add_filter('WpHelpers::QueryHelper/buildArguments', [static::class, 'setPostTypePostsPerPage'], 10, 2);
        add_filter('WpHelpers::QueryHelper/buildArguments', [static::class, 'setTaxonomyPostsPerPage'], 10, 2);
    }

    public function take($count)
    {
        if (is_numeric($count) && $count >= -1) {
            $this->args['posts_per_page'] = $count;
        }

        return $this;
    }

    public static function queryPostTypePostsPerPage($query)
    {
        if (! static::isFrontMainQuery($query) || ! $query->is_post_type_archive()) return;
        if (! $postType = get_post_type_object($query->query['post_type'])) return;
        if (! isset($postType->posts_per_page)) return;

        $query->set('posts_per_page', $postType->posts_per_page);
    }

    public static function queryTaxonomyPerPage($query)
    {
        if (! static::isFrontMainQuery($query) || ! $query->is_tax()) return;
        if (! $taxonomy = static::getOnlyTaxonomyFromQuery($query)) return;
        if (! isset($taxonomy->posts_per_page)) return;

        $query->set('posts_per_page', $taxonomy->posts_per_page);
    }

    public static function setPostTypePostsPerPage($args, $builder)
    {
        if (isset($builder->args['posts_per_page'])) return $args;
        if (! $postType = get_post_type_object($args['post_type'])) return $args;

        if ($postType && isset($postType->posts_per_page)) {
            $args['posts_per_page'] = $postType->posts_per_page;
        }

        return $args;
    }

    public static function setTaxonomyPostsPerPage($args, $builder)
    {
        if (isset($builder->args['posts_per_page'])) return $args;
        if (! $taxonomy = static::getOnyAndOnlyTaxonomyTerm($args)) return $args;

        if ($taxonomy && isset($taxonomy->posts_per_page)) {
            $args['posts_per_page'] = $taxonomy->posts_per_page;
        }

        return $args;
    }
}
