<?php

namespace Gummiforweb\WpHelpers\WpQuery\Traits;

trait QueryMeta
{
    protected function bootQueryMeta()
    {
        if (! isset($this->args['meta_query'])) $this->args['meta_query'] = [];
    }

    public function meta($key, $value, $compare = '=')
    {
        $this->args['meta_query'][] = [
            'key' => $key,
            'value' => $value,
            'compare' => $compare
        ];

        return $this;
    }

    public function metaNot($key, $value)
    {
        return $this->meta($key, $value, '!=');
    }

    public function metaIn($key, $value)
    {
        return $this->meta($key, $value, 'IN');
    }

    public function metaNotIn($key, $value)
    {
        return $this->meta($key, $value, 'NOT IN');
    }

    public function metaLike($key, $value)
    {
        return $this->meta($key, $value, 'LIKE');
    }

    public function metaNotLike($key, $value)
    {
        return $this->meta($key, $value, 'NOT LIKE');
    }

    public function metaGreater($key, $value, $include = false)
    {
        return $this->meta($key, $value, $include? '>=' : '>');
    }

    public function metaLesser($key, $value, $include = false)
    {
        return $this->meta($key, $value, $include? '<=' : '<');
    }

    public function metaExists($key)
    {
        $this->args['meta_query'][] = [
            'key' => $key,
            'compare' => 'EXISTS'
        ];

        return $this;
    }
}
