<?php

namespace Gummiforweb\WpHelpers\WpQuery\Traits;

trait QueryUtility
{
    public function count()
    {
        return $this->getWpQuery()->post_count;
    }

    public function total()
    {
        return $this->getWpQuery()->found_posts;
    }

    public function isEmpty()
    {
        return $this->count() == 0;
    }

    public function currentIndex()
    {
        return $this->getWpQuery()->current_post;
    }

    public function isOdd()
    {
        return $this->getWpQuery()->current_post % 2 == 0;
    }

    public function isEven()
    {
        return ! $this->isOdd();
    }

    public function currentRowIndex($perRow)
    {
        return $this->currentIndex() % $perRow + 1;
    }
}
