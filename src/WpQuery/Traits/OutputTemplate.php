<?php

namespace Gummiforweb\WpHelpers\WpQuery\Traits;

trait OutputTemplate
{
    protected static $loopTemplateArgs = [
        'before_loop' => '', 'after_loop' => '',
        'before_item' => '', 'after_item' => '',
    ];

    public function loop($path, $args = [])
    {
        if (! $this->have_posts()) return '';

        $args = $this->prepareTemplateArgs($args);

        echo $args['before_loop'];
        while ($this->have_posts()): $this->the_post();
            echo $args['before_item'];
            get_template_part($this->prepareTemplatePath($path));
            echo $args['after_item'];
        endwhile; wp_reset_query();
        echo $args['after_loop'];

        return $this;
    }

    public static function setTemplateLoop($args)
    {
        foreach ($args as $key => $value) {
            if (! isset(static::$loopTemplateArgs[$key])) continue;

            static::$loopTemplateArgs[$key] = $value;
        }
    }

    public static function clearTemplateLoop()
    {
        static::setTemplateLoop([
            'before_loop' => '', 'after_loop' => '',
            'before_item' => '', 'after_item' => '',
        ]);
    }

    protected function prepareTemplatePath($path)
    {
        return str_replace(['.php', '.'], ['', '/'], $path);
    }

    protected function prepareTemplateArgs($args)
    {
        if ($args === false) {
            return [
                'before_loop' => '', 'after_loop' => '',
                'before_item' => '', 'after_item' => '',
            ];
        }

        if (is_string($args)) {
            $args = ['before_loop' => $args];
        }

        return wp_parse_args($args? : [], static::$loopTemplateArgs);
    }
}
