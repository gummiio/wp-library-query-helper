<?php

namespace Gummiforweb\WpHelpers\WpQuery\Traits;

trait QueryIterator
{
    protected $iteratorStarted = false;

    public function rewind()
    {
        $this->getWpQuery();
        $this->wpQuery->rewind_posts();
        $this->iteratorStarted = false;
    }

    public function current()
    {
        return $this->wpQuery->post;
    }

    public function key()
    {
        return $this->wpQuery->current_post;
    }

    public function next()
    {
        $this->wpQuery->the_post();
    }

    public function valid()
    {
        if (! $this->iteratorStarted) {
            $this->wpQuery->the_post();
            $this->iteratorStarted = true;
        }

        return $this->wpQuery->have_posts();
    }
}
