<?php

namespace Gummiforweb\WpHelpers\WpQuery\Traits;

trait QuerySearch
{
    protected static $searchUrlKey = 'search';
    protected $autoSearch = false;

    public static function hookQuerySearch()
    {
        add_action('pre_get_posts', [static::class, 'querySearchKeywords']);
        add_action('pre_get_posts', [static::class, 'queryTaxonomyUrlString']);

        add_filter('WpHelpers::QueryHelper/buildArguments', [static::class, 'setSearchKeywords'], 10, 2);
        add_filter('WpHelpers::QueryHelper/buildArguments', [static::class, 'setTaxonomyUrlString'], 10, 2);
    }

    public function searchText($string = '')
    {
        if ($string = trim($string)) {
            $this->args['s'] = $string;
        }

        return $this;
    }

    public function search()
    {
        $this->autoSearch = true;

        return $this;
    }

    public static function querySearchKeywords($query)
    {
        if (! static::isFrontMainQuery($query)) return;
        if ($query->get('s')) return;
        if (! $value = static::getSearchKeywordValue()) return;

        $query->set('s', $value);
    }

    public static function setSearchKeywords($args, $builder)
    {
        if (! $builder->autoSearch) return $args;
        if (isset($builder->args['s'])) return $args;
        if (! $value = static::getSearchKeywordValue()) return $args;

        $args['s'] = $value;

        return $args;
    }

    public static function queryTaxonomyUrlString($query)
    {
        if (! static::isFrontMainQuery($query)) return;
        if (! $taxonomies = static::getUrlTaxonomies()) return;

        $tax_query = $query->get('tax_query')? : [];

        foreach ($taxonomies as $taxonomy => $slug) {
            $tax_query[] = [
                'taxonomy' => $taxonomy,
                'field' => 'slug',
                'terms' => $slug
            ];
        }

        $query->set('tax_query', $tax_query);
    }

    public static function setTaxonomyUrlString($args, $builder)
    {
        if (! $builder->autoSearch) return $args;
        if (! $taxonomies = static::getUrlTaxonomies()) return $args;

        $tax_query = isset($args['tax_query'])? $args['tax_query'] : [];

        foreach ($taxonomies as $taxonomy => $slug) {
            $tax_query[] = [
                'taxonomy' => $taxonomy,
                'field' => 'slug',
                'terms' => $slug
            ];
        }

        $args['tax_query'] = $tax_query;

        return $args;
    }

    protected static function getSearchKeywordValue()
    {
        if (! isset($_GET[static::$searchUrlKey])) return '';

        return trim($_GET[static::$searchUrlKey]);
    }

    protected static function getUrlTaxonomies()
    {
        $urlTaxonomies = array_values(array_intersect(get_taxonomies(), array_keys($_GET)));
        $taxonomyPairs = [];

        foreach ($urlTaxonomies as $taxonomy) {
            $taxonomyPairs[$taxonomy] = trim($_GET[$taxonomy]);
        }

        return array_filter($taxonomyPairs);
    }
}
