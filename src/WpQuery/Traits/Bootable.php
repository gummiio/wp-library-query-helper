<?php

namespace Gummiforweb\WpHelpers\WpQuery\Traits;

trait Bootable
{
    public static function staticBoot($prefix)
    {
        $class = static::class;

        foreach (static::class_uses_recursive($class) as $trait) {
            if (method_exists($class, $method = $prefix . static::class_basename($trait))) {
                forward_static_call([$class, $method]);
            }
        }
    }

    public function boot($prefix)
    {
        foreach (static::class_uses_recursive($this) as $trait) {
            if (method_exists($this, $method = $prefix . static::class_basename($trait))) {
                $this->$method();
            }
        }
    }

    protected static function class_basename($class)
    {
        $class = is_object($class)? get_class($class) : $class;

        return basename(str_replace('\\', '/', $class));
    }

    protected static function class_uses_recursive($class)
    {
        if (is_object($class)) {
            $class = get_class($class);
        }

        $results = [];

        foreach (array_merge([$class => $class], class_parents($class)) as $class) {
            $results += static::trait_uses_recursive($class);
        }

        return array_unique($results);
    }

    protected static function trait_uses_recursive($trait)
    {
        $traits = class_uses($trait);

        foreach ($traits as $trait) {
            $traits += static::trait_uses_recursive($trait);
        }

        return $traits;
    }
}
