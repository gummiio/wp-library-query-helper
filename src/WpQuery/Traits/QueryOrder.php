<?php

namespace Gummiforweb\WpHelpers\WpQuery\Traits;

trait QueryOrder
{
    public static function hookQueryOrder()
    {
        add_action('pre_get_posts', [static::class, 'queryPostTypeOrderBy']);
        add_action('pre_get_posts', [static::class, 'queryPostTypeOrder']);

        add_filter('WpHelpers::QueryHelper/buildArguments', [static::class, 'setPostTypeOrderBy'], 10, 2);
        add_filter('WpHelpers::QueryHelper/buildArguments', [static::class, 'setPostTypeOrder'], 10, 2);
    }

    public function orderby($orderby, $order = null)
    {
        $this->args['orderby'] = $orderby;
        $this->order($order);

        return $this;
    }

    public function order($order)
    {
        $order = strtolower($order);

        if (in_array($order, ['asc', 'desc'])) {
            $this->args['order'] = $order;
        } elseif (in_array($order, ['<', '+'])) {
            $this->args['order'] = 'asc';
        } elseif (in_array($order, ['>', '-'])) {
            $this->args['order'] = 'desc';
        }

        return $this;
    }

    public function latest()
    {
        return $this->orderby('date', 'desc');
    }

    public function alphabetical()
    {
        return $this->orderby('title', 'asc');
    }

    public function alpha()
    {
        return $this->alphabetical();
    }

    public function random()
    {
        return $this->orderby('rand');
    }

    public static function queryPostTypeOrderBy($query)
    {
        if (! static::isFrontMainQuery($query) || ! $query->is_post_type_archive()) return;
        if (! $postType = get_post_type_object($query->query['post_type'])) return;
        if (! isset($postType->orderby)) return;

        $query->set('orderby', $postType->orderby);
    }

    public static function queryPostTypeOrder($query)
    {
        if (! static::isFrontMainQuery($query) || ! $query->is_post_type_archive()) return;
        if (! $postType = get_post_type_object($query->query['post_type'])) return;
        if (! isset($postType->order)) return;

        $query->set('order', $postType->order);
    }

    public static function setPostTypeOrderBy($args, $builder)
    {
        if (isset($builder->args['orderby'])) return $args;

        $postType = get_post_type_object($args['post_type']);

        if ($postType && isset($postType->orderby)) {
            $args['orderby'] = $postType->orderby;
        }

        return $args;
    }

    public static function setPostTypeOrder($args, $builder)
    {
        if (isset($builder->args['order'])) return $args;

        $postType = get_post_type_object($args['post_type']);

        if ($postType && isset($postType->order)) {
            $args['order'] = $postType->order;
        }

        return $args;
    }
}
