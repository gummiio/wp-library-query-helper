<?php

namespace Gummiforweb\WpHelpers\WpQuery\Traits;

trait OutputSearchComponents
{
    public function searchTaxonomyOptions($taxonomy, $overwriteKey = '')
    {
        return $this->searchTaxonomy($taxonomy, $overwriteKey, function($term, $current) {
            return sprintf(
                '<option value="%s" %s>%s</option>',
                $term->slug,
                $term->slug == $current? 'selected' : '',
                $term->name
            );
        });
    }

    public function searchTaxonomyLists($taxonomy, $overwriteKey = '', $className = 'current')
    {
        return $this->searchTaxonomy($taxonomy, $overwriteKey, function($term, $current) use ($className) {
            return sprintf(
                '<li class="%s"><a href="%s">%s</a></li>',
                $term->slug == $current? $className : '',
                get_term_link($term),
                $term->name
            );
        });
    }

    public function searchTaxonomy($taxonomy, $overwriteKey, $fn)
    {
        $taxonomy = get_taxonomy($taxonomy);
        $terms = get_terms(['taxonomy' => $taxonomy->name]);
        $options = [];

        if ((is_tax() || is_category() || is_tag()) && get_queried_object()->taxonomy == $taxonomy->name) {
            $current = get_queried_object()->slug;
        } else {
            $current = $this->searchGet($overwriteKey? : $taxonomy->name);
        }

        foreach ($terms as $term) {
            $options[] = $fn($term, $current);
        }

        return implode("\n", $options);
    }

    public function searchInput($overwriteKey = null, $class = '', $placeholder = '')
    {
        $searchKey = $overwriteKey? : static::$searchUrlKey;

        return sprintf('<input type="text" name="%s" value="%s" placeholder="%s" class="%s" />',
            $searchKey,
            $this->searchGet($searchKey),
            $placeholder,
            $class
        );
    }

    protected function searchGet($key, $default = null)
    {
        if (! isset($_GET[$key])) return $default;

        return $_GET[$key];
    }
}
