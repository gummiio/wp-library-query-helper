<?php

namespace Gummiforweb\WpHelpers\WpQuery\Traits;

trait QueryRelated
{
    protected $relatedPostType;
    protected $relatedTaxonomyTerms = [];
    protected $relatedToPost = null;
    protected $excludedTermIds = [];
    protected $excludedPostIds = [];

    public static function hookQueryRelated()
    {
        add_filter('WpHelpers::QueryHelper/buildArguments', [static::class, 'setPostRelatedQuery'], 10, 2);
    }

    public function related($relatedPostType = 'post', $relatedTaxonomies = 'category', $relatedToPost = null)
    {
        $this->wpQuery = null; // in case of calling from global object
        $this->relatedPostType = $relatedPostType;
        $this->relatedTo($relatedToPost);

        foreach (is_array($relatedTaxonomies)? $relatedTaxonomies : [$relatedTaxonomies] as $taxonomy) {
            $this->relatedTaxonomyTerms[$taxonomy] = []; // query this later
        }

        return $this;
    }

    public function relatedTo($relatedToPost = null)
    {
        if (! $relatedToPost && is_singular() && ! is_page() && ! is_attachment()) {
            $relatedToPost = get_queried_object_id();
        }

        if ($relatedToPost) {
            $this->relatedToPost = get_post($relatedToPost);
            $this->excludedPostIds[] = $this->relatedToPost->ID;
        }

        return $this;
    }

    public function excludeTerms($terms = [])
    {
        foreach (is_array($terms)? $terms : [$terms] as $term) {
            $term = get_term($term);
            $this->excludedTermIds[] = $term->term_id;
        }

        $this->excludedTermIds = array_filter(array_unique($this->excludedTermIds));

        return $this;
    }

    public function excludePosts($posts = [])
    {
        foreach (is_array($posts)? $posts : [$posts] as $post) {
            $post = get_post($post);
            $this->excludedPostIds[] = $post->ID;
        }

        $this->excludedPostIds = array_filter(array_unique($this->excludedPostIds));

        return $this;
    }

    public static function setPostRelatedQuery($args, $builder)
    {
        if (! $builder->relatedPostType && ! $builder->relatedToPost && ! $builder->relatedTaxonomyTerms) return $args;

        if (! $builder->relatedToPost) {
            $args['post__in'] = [0]; // need empty array for this
            return $args;
        }

        $totalTaxonomies = count($builder->relatedTaxonomyTerms);
        $totalEmptyTaxonomyTerms = 0;
        $taxQuery = [];

        foreach ($builder->relatedTaxonomyTerms as $taxonomy => $_) {
            if (! $terms = get_the_terms($builder->relatedToPost, $taxonomy)) {
                $totalEmptyTaxonomyTerms ++;
                $termIds = [0];
            } else {
                $termIds = wp_list_pluck($terms, 'term_id');
            }

            $taxQuery[] = [
                'taxonomy' => $taxonomy,
                'field' => 'term_id',
                'terms' => array_diff($termIds, $builder->excludedTermIds)
            ];
        }

        if ($totalEmptyTaxonomyTerms >= $totalTaxonomies) {
            $args['post__in'] = [0]; // not empty array for this
            return $args;
        }

        $args['post_type'] = $builder->relatedPostType;
        $args['post__not_in'] = $builder->excludedPostIds;

        $args['tax_query'] = isset($args['tax_query'])? $args['tax_query'] : [];
        $args['tax_query'] = array_merge($args['tax_query'], $taxQuery);

        return $args;
    }
}
