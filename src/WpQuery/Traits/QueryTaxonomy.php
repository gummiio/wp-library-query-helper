<?php

namespace Gummiforweb\WpHelpers\WpQuery\Traits;

trait QueryTaxonomy
{
    protected function bootQuueryTaxonomy()
    {
        if (! isset($this->args['tax_query'])) $this->args['tax_query'] = [];
    }

    public function taxonomy($terms, $taxonomy = '')
    {
        if (! $terms = $this->convertTermsArray($terms, $taxonomy)) return $this;

        foreach ($terms as $term) {
            $this->args['tax_query'][] = [
                'taxonomy' => $term->taxonomy,
                'field' => 'term_id',
                'terms' => $term->term_id
            ];
        }

        return $this;
    }

    public function taxonomyIn($terms, $taxonomy = '')
    {
        if (! $terms = $this->convertTermsArray($terms, $taxonomy, true)) return $this;

        $this->args['tax_query'][] = [
            'taxonomy' => $terms[0]->taxonomy,
            'field' => 'term_id',
            'terms' => wp_list_pluck($terms, 'term_id')
        ];

        return $this;
    }

    protected function convertTermsArray($terms, $taxonomy = '', $whereIn = false)
    {
        return array_filter(array_map(function($term) use ($taxonomy, $whereIn) {
            if (! $term = $this->getTerm($term, $taxonomy)) return null;
            if ($whereIn && ! $taxonomy) $taxonomy = $term->taxonomy;
            return $term;
        }, is_array($terms)? $terms : [$terms]));
    }

    protected function getTerm($term, $taxonomy = '')
    {
        if (is_string($term)) {
            return get_term_by('slug', $term, $taxonomy);
        }

        if (! ($term = get_term($term, $taxonomy)) || is_wp_error($term)) {
            return false;
        }

        return $term;
    }

    protected static function getOnyAndOnlyTaxonomyTerm($args)
    {
        if (! isset($args['tax_query'])) return false;
        if (count($args['tax_query']) > 1) return false;
        if (count($args['tax_query'][0]['terms']) > 1) return false;

        return get_taxonomy($args['tax_query'][0]['taxonomy']);
    }

    protected static function getOnlyTaxonomyFromQuery($query)
    {
        if (! $taxonomies = array_intersect(get_taxonomies(), array_keys($query->query))) return false;
        if (count($taxonomies) > 1) return false;

        return $taxonomy = get_taxonomy(array_shift($taxonomies));
    }
}
