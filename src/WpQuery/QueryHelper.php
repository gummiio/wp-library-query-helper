<?php

namespace Gummiforweb\WpHelpers\WpQuery;

use Gummiforweb\WpHelpers\WpQuery\Traits\Bootable;
use Gummiforweb\WpHelpers\WpQuery\Traits\OutputPagination;
use Gummiforweb\WpHelpers\WpQuery\Traits\OutputSearchComponents;
use Gummiforweb\WpHelpers\WpQuery\Traits\OutputTemplate;
use Gummiforweb\WpHelpers\WpQuery\Traits\QueryIterator;
use Gummiforweb\WpHelpers\WpQuery\Traits\QueryLimit;
use Gummiforweb\WpHelpers\WpQuery\Traits\QueryMeta;
use Gummiforweb\WpHelpers\WpQuery\Traits\QueryOrder;
use Gummiforweb\WpHelpers\WpQuery\Traits\QueryRelated;
use Gummiforweb\WpHelpers\WpQuery\Traits\QuerySearch;
use Gummiforweb\WpHelpers\WpQuery\Traits\QueryTaxonomy;
use Gummiforweb\WpHelpers\WpQuery\Traits\QueryUtility;
use Iterator;
use WP_Query;

class QueryHelper implements Iterator
{
    use Bootable, QueryIterator, QueryLimit, QueryOrder, QueryTaxonomy, QueryMeta,
        QueryRelated, QuerySearch, QueryUtility,
        OutputTemplate, OutputPagination, OutputSearchComponents;

    protected static $lastInstance = null;

    protected $wpQuery = null;
    protected $args = [];
    protected $built = false;

    public function __construct($useGlobal = false)
    {
        $this->boot('boot');

        if ($useGlobal) $this->getGlobal();
    }

    public static function initHooks()
    {
        (new static)->staticBoot('hook');
    }

    public static function clearCache()
    {
        static::$lastInstance = null;
    }

    public function cache()
    {
        static::$lastInstance = $this;

        return $this;
    }

    public function last()
    {
        return static::$lastInstance? : new static(true);
    }

    public function getGlobal()
    {
        $this->wpQuery = $GLOBALS['wp_query'];

        return $this;
    }

    public function query($postType = 'post', $args = [])
    {
        $this->args = wp_parse_args(
            $this->args,
            wp_parse_args(
                is_numeric($args)? ['posts_per_page' => $args] : $args,
                ['post_type' => $postType]
            )
        );

        return $this;
    }

    public function getWpQuery()
    {
        if (! $this->wpQuery) {
            $this->wpQuery = new WP_Query();
            $this->wpQuery->query($this->buildArguments());
        }

        return $this->wpQuery;
    }

    protected function buildArguments()
    {
        // dump(apply_filters('WpHelpers::QueryHelper/buildArguments', $this->args, $this));
        return apply_filters('WpHelpers::QueryHelper/buildArguments', wp_parse_args([
            'post_type' => isset($this->args['post_type'])? $this->args['post_type'] : 'post',
            'paged' => isset($this->args['paged'])? $this->args['paged'] : get_query_var('paged', 1)
        ], $this->args), $this);
    }

    protected static function isFrontMainQuery($query)
    {
        return $query->is_main_query() && ! $query->is_admin() && ! ($query->is_page() || $query->is_singular());
    }

    protected function triggerError($type, $value)
    {
        $trace = debug_backtrace();

        trigger_error(
            sprintf(
                'Undefined %s <b>`%s`</b> in <i>%s</i> on line <i>%s</i>',
                $type, $value, $trace[1]['file'], $trace[1]['line']
            ),
            E_USER_NOTICE
        );
    }

    public function __call($method, $args)
    {
        if (method_exists($this->getWpQuery(), $method)) {
            return call_user_func_array([$this->wpQuery, $method], $args);
        }

        $this->triggerError('method', $method);

        return null;
    }

    public function __get($key)
    {
        if (isset($this->getWpQuery()->$key)) {
            return $this->wpQuery->$key;
        }

        $this->triggerError('property', $key);
    }

    public function __isset($key)
    {
        return isset($this->getWpQuery()->$key);
    }
}


/*
 |--------------------------------------------------------------------------
 | content nav
 |--------------------------------------------------------------------------
*/
