<?php

use Gummiforweb\WpHelpers\WpQuery\QueryHelper;

class SearchTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        // create some dummy post just so test is more accurate
        $this->createPosts(20, ['post_type' => 'post']);
    }

    /** @test */
    public function query_will_not_automatically_pickup_keywords_from_url()
    {
        $post = $this->newPost('post', ['post_title' => 'ABCDE']);
        $this->go_to(add_query_arg(['search' => 'ABCDE'], home_url()));

        $query = new QueryHelper;

        $this->assertCount(10, $query->posts);
    }

    /** @test */
    public function search_query_will__pickup_keywords_from_url()
    {
        $post = $this->newPost('post', ['post_title' => 'ABCDE']);
        $this->go_to(add_query_arg(['search' => 'ABCDE'], home_url()));

        $query = (new QueryHelper)->search();

        $this->assertCount(1, $query->posts);
        $this->assertEquals($post->ID, $query->post->ID);
    }

    /** @test */
    public function search_query_will_auto_invoke_on_archive_main_query()
    {
        $post = $this->newPost('post', ['post_title' => 'ABCDE']);
        $this->go_to(add_query_arg(['search' => 'ABCDE'], home_url()));

        $query = new QueryHelper(true);

        $this->assertTrue($query->is_home());
        $this->assertCount(1, $query->posts);
        $this->assertEquals($post->ID, $query->post->ID);
    }

    /** @test */
    public function search_query_will_not_auto_invoke_on_page_main_query()
    {
        $page = $this->newPost('page');
        $post = $this->newPost('post', ['post_title' => 'ABCDE']);
        $this->go_to(add_query_arg(['search' => 'ABCDE'], get_permalink($page->ID)));

        $query = new QueryHelper(true);

        $this->assertTrue($query->is_page());
        $this->assertCount(1, $query->posts);
        $this->assertEquals($page->ID, $query->post->ID);
    }

    /** @test */
    public function search_query_will_auto_invoke_on_archive_page()
    {
        $post = $this->newPost('post', ['post_title' => 'ABCDE']);
        $term = $this->newTerm('category');
        wp_set_post_terms($post->ID, [$term->term_id], 'category');
        $this->go_to(add_query_arg(['search' => 'ABCDE'], get_term_link($term)));

        $query = new QueryHelper(true);

        $this->assertTrue($query->is_category());
        $this->assertCount(1, $query->posts);
        $this->assertEquals($post->ID, $query->post->ID);
    }

    /** @test */
    public function it_can_use_search_to_search_without_from_url()
    {
        $post = $this->newPost('post', ['post_title' => 'ABCDE']);

        $query = (new QueryHelper)->searchText('ABCD');
        $query2 = (new QueryHelper)->query('post', ['s' => 'ABCD']);

        $this->assertCount(1, $query->posts);
        $this->assertEquals($post->ID, $query->post->ID);

        $this->assertCount(1, $query2->posts);
        $this->assertEquals($post->ID, $query2->post->ID);
    }

    /** @test */
    public function search_will_trump_url_query()
    {
        $this->newPost('post', ['post_title' => 'ABCDE']);
        $post = $this->newPost('post', ['post_title' => 'GHIJK']);
        $this->go_to(add_query_arg(['search' => 'ABCDE'], home_url()));

        $query = (new QueryHelper)->search()->searchText('GHI');
        $query2 = (new QueryHelper)->search()->query('post', ['s' => 'GHI']);

        $this->assertCount(1, $query->posts);
        $this->assertEquals($post->ID, $query->post->ID);

        $this->assertCount(1, $query2->posts);
        $this->assertEquals($post->ID, $query2->post->ID);
    }

    /** @test */
    public function query_will_not_automatically_pickup_taxonomy_from_url()
    {
        $post = $this->newPost('post', ['post_title' => 'ABCDE']);
        $term = $this->newTerm('category');
        wp_set_post_terms($post->ID, [$term->term_id], 'category');
        $this->go_to(add_query_arg(['category' => $term->slug], home_url()));

        $query = new QueryHelper;

        $this->assertCount(10, $query->posts);
    }

    /** @test */
    public function query_will_automatically_pickup_taxonomy_from_url()
    {
        $post = $this->newPost('post', ['post_title' => 'ABCDE']);
        $term = $this->newTerm('category');
        wp_set_post_terms($post->ID, [$term->term_id], 'category');
        $this->go_to(add_query_arg(['category' => $term->slug], home_url()));

        $query = (new QueryHelper)->search();

        $this->assertCount(1, $query->posts);
        $this->assertEquals($post->ID, $query->post->ID);
    }

    /** @test */
    public function query_will_automatically_pickup_taxonomy_on_archive_main_query()
    {
        $post = $this->newPost('post', ['post_title' => 'ABCDE']);
        $term = $this->newTerm('category');
        wp_set_post_terms($post->ID, [$term->term_id], 'category');
        $this->go_to(add_query_arg(['category' => $term->slug], home_url()));

        $query = new QueryHelper(true);

        $this->assertTrue($query->is_home());
        $this->assertCount(1, $query->posts);
        $this->assertEquals($post->ID, $query->post->ID);
    }

    /** @test */
    public function query_will_not_auto_pickup_taxonomy_on_page_main_query()
    {
        $page = $this->newPost('page');
        $post = $this->newPost('post', ['post_title' => 'ABCDE']);
        $term = $this->newTerm('category');
        wp_set_post_terms($post->ID, [$term->term_id], 'category');
        $this->go_to(add_query_arg(['category' => $term->slug], get_permalink($page->ID)));

        $query = new QueryHelper(true);

        $this->assertTrue($query->is_page());
        $this->assertCount(1, $query->posts);
        $this->assertEquals($page->ID, $query->post->ID);
    }

    /** @test */
    public function query_will_automatically_pickup_taxonomy_from_archive_page()
    {
        $this->creaatePostsWithTerm(10, 'post', $term = $this->newTerm('category'));
        $post = $this->newPost('post', ['post_title' => 'ABCDE']);
        $term2 = $this->newTerm('category');
        wp_set_post_terms($post->ID, [$term->term_id, $term2->term_id], 'category');
        $this->go_to(add_query_arg(['category' => $term2->slug], get_term_link($term)));

        $query = new QueryHelper(true);

        $this->assertTrue($query->is_category());
        $this->assertCount(1, $query->posts);
        $this->assertEquals($post->ID, $query->post->ID);
    }

    /** @test */
    public function taxonomy_url_query_can_have_multile_taxonomies()
    {
        $postIds = $this->creaatePostsWithTerm(3, 'post', $term = $this->newTerm('category'));
        $tag = $this->newTerm('post_tag');
        foreach ($postIds as $postId) {
            wp_set_post_terms($postId, [$tag->term_id], 'post_tag', true);
        }
        $this->go_to(add_query_arg([
            'category' => $term->slug,
            'post_tag' => $tag->slug
        ], home_url()));

        $query = (new QueryHelper)->search();

        $this->assertCount(3, $query->posts);
        $this->assertEquals($postIds, array_reverse(wp_list_pluck($query->posts, 'ID')));
    }
}
