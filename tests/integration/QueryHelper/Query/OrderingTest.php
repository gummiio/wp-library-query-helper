<?php

use Carbon\Carbon;
use Gummiforweb\WpHelpers\WpQuery\QueryHelper;

class OrderingTest extends TestCase
{
    /** @test */
    public function orderby_can_be_set_when_registering_post_type()
    {
        $this->registerPostType('service', ['orderby' => 'menu_order']);
        list($post1, $post2, $post3, $post4) = $this->creaatePostTypeWithOrder('service', 'menu_order', [5, 9, 6, 2]);

        $query = (new QueryHelper)->query('service');

        $this->assertEquals([$post2, $post3, $post1, $post4], wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function order_can_be_set_when_registering_post_type()
    {
        $this->registerPostType('service', ['orderby' => 'menu_order', 'order' => 'asc']);
        list($post1, $post2, $post3, $post4) = $this->creaatePostTypeWithOrder('service', 'menu_order', [5, 9, 6, 2]);

        $query = (new QueryHelper)->query('service');

        $this->assertEquals([$post4, $post1, $post3, $post2], wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function post_type_orderby_will_set_on_post_type_archive_page()
    {
        $this->registerPostType('service', ['orderby' => 'menu_order', 'has_archive' => true]);
        list($post1, $post2, $post3, $post4) = $this->creaatePostTypeWithOrder('service', 'menu_order', [5, 9, 6, 2]);
        $this->go_to(get_post_type_archive_link('service'));

        $query = new QueryHelper(true);

        $this->assertEquals([$post2, $post3, $post1, $post4], wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function post_type_order_will_set_on_post_type_archive_page()
    {
        $this->registerPostType('service', ['orderby' => 'menu_order', 'order' => 'asc', 'has_archive' => true]);
        list($post1, $post2, $post3, $post4) = $this->creaatePostTypeWithOrder('service', 'menu_order', [5, 9, 6, 2]);
        $this->go_to(get_post_type_archive_link('service'));

        $query = new QueryHelper(true);

        $this->assertEquals([$post4, $post1, $post3, $post2], wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function order_and_orderby_on_taxonomy_should_not_effect_post_order()
    {
        $this->registerPostType('service');
        $this->registerTaxonomy('service_type', 'service', ['order' => 'asc']);
        $term = $this->newTerm('service_type');
        list($post1, $post2, $post3, $post4) = $this->creaatePostTypeWithOrder('service', 'menu_order', [5, 9, 6, 2]);
        wp_set_post_terms($post4, [$term->term_id], 'service_type');
        wp_set_post_terms($post1, [$term->term_id], 'service_type');
        wp_set_post_terms($post2, [$term->term_id], 'service_type');
        wp_set_post_terms($post3, [$term->term_id], 'service_type');

        $query = (new QueryHelper)->query('service')->taxonomy($term)->orderby('menu_order');

        $this->assertEquals([$post2, $post3, $post1, $post4], wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function orderby_can_get_set_as_arg_or_orderby_method()
    {
        $this->registerPostType('service');
        list($post1, $post2, $post3, $post4) = $this->creaatePostTypeWithOrder('service', 'menu_order', [5, 9, 6, 2]);

        $query = (new QueryHelper)->query('service', ['orderby' => 'menu_order']);
        $query2 = (new QueryHelper)->query('service')->orderby('menu_order');

        $this->assertEquals([$post2, $post3, $post1, $post4], wp_list_pluck($query->posts, 'ID'));
        $this->assertEquals([$post2, $post3, $post1, $post4], wp_list_pluck($query2->posts, 'ID'));
    }

    /** @test */
    public function order_and_shorthand_can_get_set_as_arg_by_order_and_orderby_method()
    {
        $this->registerPostType('service');
        list($post1, $post2, $post3, $post4) = $this->creaatePostTypeWithOrder('service', 'menu_order', [5, 9, 6, 2]);

        $query = (new QueryHelper)->query('service')->order('asc');
        $query2 = (new QueryHelper)->query('service')->order('<');
        $query3 = (new QueryHelper)->query('service')->order('+');

        $query4 = (new QueryHelper)->query('service')->order('desc');
        $query5 = (new QueryHelper)->query('service')->order('>');
        $query6 = (new QueryHelper)->query('service')->order('-');

        $query7 = (new QueryHelper)->query('service')->orderby('date', 'asc');
        $query8 = (new QueryHelper)->query('service')->orderby('date', '<');
        $query9 = (new QueryHelper)->query('service')->orderby('date', '+');

        $query10 = (new QueryHelper)->query('service')->orderby('date', 'desc');
        $query11 = (new QueryHelper)->query('service')->orderby('date', '>');
        $query12 = (new QueryHelper)->query('service')->orderby('date', '-');

        $this->assertEquals([$post1, $post2, $post3, $post4], wp_list_pluck($query->posts, 'ID'));
        $this->assertEquals([$post1, $post2, $post3, $post4], wp_list_pluck($query2->posts, 'ID'));
        $this->assertEquals([$post1, $post2, $post3, $post4], wp_list_pluck($query3->posts, 'ID'));

        $this->assertEquals([$post4, $post3, $post2, $post1], wp_list_pluck($query4->posts, 'ID'));
        $this->assertEquals([$post4, $post3, $post2, $post1], wp_list_pluck($query5->posts, 'ID'));
        $this->assertEquals([$post4, $post3, $post2, $post1], wp_list_pluck($query6->posts, 'ID'));

        $this->assertEquals([$post1, $post2, $post3, $post4], wp_list_pluck($query7->posts, 'ID'));
        $this->assertEquals([$post1, $post2, $post3, $post4], wp_list_pluck($query8->posts, 'ID'));
        $this->assertEquals([$post1, $post2, $post3, $post4], wp_list_pluck($query9->posts, 'ID'));

        $this->assertEquals([$post4, $post3, $post2, $post1], wp_list_pluck($query10->posts, 'ID'));
        $this->assertEquals([$post4, $post3, $post2, $post1], wp_list_pluck($query11->posts, 'ID'));
        $this->assertEquals([$post4, $post3, $post2, $post1], wp_list_pluck($query12->posts, 'ID'));
    }

    /** @test */
    public function latest_method_will_orderby_to_desc_date()
    {
        $this->registerPostType('service');
        list($post1, $post2, $post3, $post4) = $this->creaatePostTypeWithOrder('service', 'menu_order', [5, 9, 6, 2]);

        $query = (new QueryHelper)->query('service')->latest();

        $this->assertEquals([$post4, $post3, $post2, $post1], wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function alphabetical_method_will_set_orderby_to_name_asc()
    {
        $this->registerPostType('service');
        $post1 = $this->newPost('service', ['post_title' => 'B title']);
        $post2 = $this->newPost('service', ['post_title' => 'A title']);
        $post3 = $this->newPost('service', ['post_title' => 'C title']);

        $query = (new QueryHelper)->query('service')->alphabetical();
        $query2 = (new QueryHelper)->query('service')->alpha();

        $this->assertEquals([$post2->ID, $post1->ID, $post3->ID], wp_list_pluck($query->posts, 'ID'));
        $this->assertEquals([$post2->ID, $post1->ID, $post3->ID], wp_list_pluck($query2->posts, 'ID'));
    }

    /** @test */
    public function random_method_will_set_orderby_to_random()
    {
        $this->registerPostType('service');
        $postIds = $this->createPosts(20, ['post_type' => 'service']);

        $query = (new QueryHelper)->query('service')->random();
        $query2 = (new QueryHelper)->query('service')->random();

        $this->assertNotEquals(wp_list_pluck($query->posts, 'ID'), wp_list_pluck($query2->posts, 'ID'));
    }
}
