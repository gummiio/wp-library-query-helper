<?php

use Gummiforweb\WpHelpers\WpQuery\QueryHelper;

class MetaTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        // create some dummy post just so test is more accurate
        $this->registerPostType('service');
        $this->createPosts(20, ['post_type' => 'post']);
    }

    /** @test */
    public function it_will_query_post_with_matching_meta_value()
    {
        $this->createPostWithKey(['my_key' => 'His Value']);
        $post = $this->createPostWithKey(['my_key' => 'My Value']);

        $query = (new QueryHelper)->meta('my_key', 'My Value');

        $this->assertCount(1, $query->posts);
        $this->assertEquals($post->ID, $query->post->ID);
    }

    /** @test */
    public function comparison_can_be_changed_as_third_parameter()
    {
        $post1 = $this->createPostWithKey(['my_key' => 10]);
        $post2 = $this->createPostWithKey(['my_key' => 30]);

        $query = (new QueryHelper)->meta('my_key', 20, '>');

        $this->assertCount(1, $query->posts);
        $this->assertEquals($post2->ID, $query->post->ID);
    }

    /** @test */
    public function shorthand__mataNot__will_exclude_matching_value()
    {
        $post1 = $this->createPostWithKey(['my_key' => 'abc'], 'service');
        $post2 = $this->createPostWithKey(['my_key' => 'cde'], 'service');
        $post3 = $this->createPostWithKey(['my_key' => 'efg'], 'service');

        $query = (new QueryHelper)->metaNot('my_key', 'abc')->query('service');

        $this->assertCount(2, $query->posts);
        $this->assertNotContains($post1->ID, wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function shorthand__mataIn__will_match_value_in_array()
    {
        $post1 = $this->createPostWithKey(['my_key' => 'a'], 'service');
        $post2 = $this->createPostWithKey(['my_key' => 'b'], 'service');
        $post3 = $this->createPostWithKey(['my_key' => 'c'], 'service');

        $query = (new QueryHelper)->metaIn('my_key', ['a', 'b'])->query('service');

        $this->assertCount(2, $query->posts);
        $this->assertNotContains($post3->ID, wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function shorthand__mataNotIn__will_exclude_value_in_array()
    {
        $post1 = $this->createPostWithKey(['my_key' => 'a'], 'service');
        $post2 = $this->createPostWithKey(['my_key' => 'b'], 'service');
        $post3 = $this->createPostWithKey(['my_key' => 'c'], 'service');

        $query = (new QueryHelper)->metaNotIn('my_key', ['a', 'b'])->query('service');

        $this->assertCount(1, $query->posts);
        $this->assertContains($post3->ID, wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function shorthand__mataLike__will_match_value_containing_string()
    {
        $post1 = $this->createPostWithKey(['my_key' => ['a', 'b', 'c', 'd', 'e']], 'service');
        $post2 = $this->createPostWithKey(['my_key' => ['c', 'd', 'e', 'f', 'g']], 'service');
        $post3 = $this->createPostWithKey(['my_key' => ['e', 'f', 'g', 'h', 'i']], 'service');

        // a:5:{i:0;s:1:"a";i:1;s:1:"b";i:2;s:1:"c";i:3;s:1:"d";i:4;s:1:"e";}
        $query = (new QueryHelper)->metaLike('my_key', ':"b";')->query('service');
        $query2 = (new QueryHelper)->metaLike('my_key', ':"d";')->query('service');
        $query3 = (new QueryHelper)->metaLike('my_key', ':"e";')->query('service');

        $this->assertCount(1, $query->posts);
        $this->assertEquals($post1->ID, $query->post->ID);

        $this->assertCount(2, $query2->posts);
        $this->assertNotContains($post3->ID, wp_list_pluck($query2->posts, 'ID'));

        $this->assertCount(3, $query3->posts);
    }

    /** @test */
    public function shorthand__mataNotLike__will_exclude_value_containing_string()
    {
        $post1 = $this->createPostWithKey(['my_key' => ['a', 'b', 'c', 'd', 'e']], 'service');
        $post2 = $this->createPostWithKey(['my_key' => ['c', 'd', 'e', 'f', 'g']], 'service');
        $post3 = $this->createPostWithKey(['my_key' => ['e', 'f', 'g', 'h', 'i']], 'service');

        $query = (new QueryHelper)->metaNotLike('my_key', ':"b";')->query('service');
        $query2 = (new QueryHelper)->metaNotLike('my_key', ':"d";')->query('service');
        $query3 = (new QueryHelper)->metaNotLike('my_key', ':"e";')->query('service');

        $this->assertCount(2, $query->posts);
        $this->assertNotContains($post1->ID, wp_list_pluck($query2->posts, 'ID'));

        $this->assertCount(1, $query2->posts);
        $this->assertContains($post3->ID, wp_list_pluck($query2->posts, 'ID'));

        $this->assertCount(0, $query3->posts);
    }

    /** @test */
    public function shorthand__mataGreater__will_match_value_in_array()
    {
        $post1 = $this->createPostWithKey(['my_key' => '10'], 'service');
        $post2 = $this->createPostWithKey(['my_key' => '20'], 'service');
        $post3 = $this->createPostWithKey(['my_key' => '30'], 'service');

        $query = (new QueryHelper)->metaGreater('my_key', 20)->query('service');
        $query2 = (new QueryHelper)->metaGreater('my_key', 20, true)->query('service');

        $this->assertCount(1, $query->posts);
        $this->assertContains($post3->ID, wp_list_pluck($query->posts, 'ID'));

        $this->assertCount(2, $query2->posts);
        $this->assertNotContains($post1->ID, wp_list_pluck($query2->posts, 'ID'));
    }

    /** @test */
    public function shorthand__mataLesser__will_exclude_value_in_array()
    {
        $post1 = $this->createPostWithKey(['my_key' => '10'], 'service');
        $post2 = $this->createPostWithKey(['my_key' => '20'], 'service');
        $post3 = $this->createPostWithKey(['my_key' => '30'], 'service');

        $query = (new QueryHelper)->metaLesser('my_key', 20)->query('service');
        $query2 = (new QueryHelper)->metaLesser('my_key', 20, true)->query('service');

        $this->assertCount(1, $query->posts);
        $this->assertContains($post1->ID, wp_list_pluck($query->posts, 'ID'));

        $this->assertCount(2, $query2->posts);
        $this->assertNotContains($post3->ID, wp_list_pluck($query2->posts, 'ID'));
    }

    /** @test */
    public function it_can_query_posts_that_has_meta_in_database()
    {
        $post1 = $this->createPostWithKey(['my_key' => '10'], 'service');
        $post2 = $this->createPostWithKey([], 'service');

        $query = (new QueryHelper)->metaExists('my_key')->query('service');

        $this->assertCount(1, $query->posts);
        $this->assertContains($post1->ID, wp_list_pluck($query->posts, 'ID'));
    }

    protected function createPostWithKey($metas, $postType = 'post', $args = [])
    {
        $post = $this->newPost($postType, $args);

        foreach ($metas as $key => $value) {
            update_post_meta($post->ID, $key, $value);
        }

        return $post;
    }
}
