<?php

use Gummiforweb\WpHelpers\WpQuery\QueryHelper;


class LimitingTest extends TestCase
{
    /** @test */
    public function it_will_return_default_posts_per_page()
    {
        $posts = $this->createPosts(20, ['post_type' => 'post']);

        $query = (new QueryHelper)->query();

        $this->assertCount(10, $query->posts);
    }

    /** @test */
    public function posts_per_page_can_be_passed_through_args_params_or_take_method()
    {
        $this->registerPostType('service');
        $this->createPosts(20, ['post_type' => 'service']);

        $query = (new QueryHelper)->query('service', ['posts_per_page' => 2]);
        $query2 = (new QueryHelper)->query('service')->take(2);
        $query3 = (new QueryHelper)->query('service', 2);

        $this->assertCount(2, $query->posts);
        $this->assertCount(2, $query2->posts);
    }

    /** @test */
    public function post_type_posts_per_page_will_be_used_on_post_type_archive()
    {
        $this->registerPostType('service', ['posts_per_page' => 5, 'has_archive' => true]);
        $this->createPosts(20, ['post_type' => 'service']);
        $this->go_to(get_post_type_archive_link('service'));

        $query = new QueryHelper(true);

        $this->assertCount(5, $query->posts);
    }

    /** @test */
    public function posts_per_page_can_be_set_when_registering_post_type()
    {
        $this->registerPostType('service', ['posts_per_page' => 5]);
        $this->createPosts(20, ['post_type' => 'service']);

        $query = (new QueryHelper)->query('service');

        $this->assertCount(5, $query->posts);
    }

    /** @test */
    public function posts_per_page_args_or_take_method_should_trump_post_type_posts_per_page()
    {
        $this->registerPostType('service', ['posts_per_page' => 5]);
        $this->createPosts(20, ['post_type' => 'service']);

        $query = (new QueryHelper)->query('service')->take(10);
        $query2 = (new QueryHelper)->query('service', ['posts_per_page' => 10]);

        $this->assertCount(10, $query->posts);
        $this->assertCount(10, $query2->posts);
    }

    /** @test */
    public function taxonomy_posts_per_page_will_be_used_on_archive_page()
    {
        $this->registerPostType('service');
        $this->registerTaxonomy('service_type', 'service', ['posts_per_page' => 5]);
        $this->creaatePostsWithTerm(20, 'service', $term = $this->newTerm('service_type'));
        $this->go_to(get_term_link($term));

        $query = new QueryHelper(true);

        $this->assertCount(5, $query->posts);
    }

    /** @test */
    public function it_should_auto_use_taxonomy_posts_per_page_if_single_term_is_queried()
    {
        $this->registerPostType('service');
        $this->registerTaxonomy('service_type', 'service', ['posts_per_page' => 5]);
        $this->creaatePostsWithTerm(20, 'service', $term = $this->newTerm('service_type'));

        $query = (new QueryHelper)->query('service')->taxonomy($term);

        $this->assertCount(5, $query->posts);
    }

    /** @test */
    public function it_should_not_auto_use_taxonomy_posts_per_page_if_multiple_terms_are_queried()
    {
        $this->registerPostType('service');
        $this->registerTaxonomy('service_type', 'service', ['posts_per_page' => 5]);
        $term = $this->newTerm('service_type');
        $term2 = $this->newTerm('service_type');
        $postIds = $this->creaatePostsWithTerm(20, 'service', $term);
        foreach ($postIds as $postId) {
            wp_set_post_terms($postId, [$term2->term_id], $term2->taxonomy, true);
        }

        $query = (new QueryHelper)->query('service')->taxonomyIn([$term, $term2]);
        $query2 = (new QueryHelper)->query('service')->taxonomy($term)->taxonomy($term2);

        $this->assertCount(10, $query->posts);
        $this->assertCount(10, $query2->posts);
    }

    /** @test */
    public function posts_per_page_args_or_take_method_should_trump_taxonomy_posts_per_page()
    {
        $this->registerPostType('service');
        $this->registerTaxonomy('service_type', 'service', ['posts_per_page' => 5]);
        $this->creaatePostsWithTerm(20, 'service', $term = $this->newTerm('service_type'));

        $query = (new QueryHelper)->query('service', ['posts_per_page' => 2])->taxonomy($term);
        $query2 = (new QueryHelper)->query('service')->taxonomy($term)->take(2);

        $this->assertCount(2, $query->posts);
        $this->assertCount(2, $query2->posts);
    }

    /** @test */
    public function post_type_posts_per_page_should_be_used_if_taxonomy_has_no_posts_per_page()
    {
        $this->registerPostType('service', ['posts_per_page' => 5]);
        $this->registerTaxonomy('service_type', 'service');
        $this->creaatePostsWithTerm(20, 'service', $term = $this->newTerm('service_type'));

        $query = (new QueryHelper)->query('service')->taxonomy($term);

        $this->assertCount(5, $query->posts);
    }
}
