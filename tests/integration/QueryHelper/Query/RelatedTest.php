<?php

use Carbon\Carbon;
use Gummiforweb\WpHelpers\WpQuery\QueryHelper;

class RelatedTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        // create some random ones to mess around
        $this->registerPostType('service', ['has_archive' => true]);
        $this->createPosts(10, ['post_type' => 'post'], (new Carbon)->subDays(2));
        $this->createPosts(10, ['post_type' => 'service'], (new Carbon)->subDays(2));

        $this->relatedTag = $tag = $this->newTerm('post_tag');
        $this->relatedPostIds = $this->creaatePostsWithTerm(5, 'post', $tag);
        $this->relatedServiceIds = $this->creaatePostsWithTerm(3, 'service', $tag);
    }

    /** @test */
    public function it_will_pull_related_posts_by_taxonomy_on_current_singular()
    {
        $this->go_to(get_permalink($this->relatedServiceIds[0]));

        $query = (new QueryHelper(true))->related('post', 'post_tag');

        $this->assertCount(5, $query->posts);
    }

    /** @test */
    public function only_singular_can_pull_related_posts_directly()
    {
        $this->go_to(get_permalink($this->newPost('page')));
        $query = (new QueryHelper(true))->related('post', 'post_tag');
        $this->assertCount(0, $query->posts);

        $this->go_to(get_post_type_archive_link('service'));
        $query = (new QueryHelper(true))->related('post', 'post_tag');
        $this->assertCount(0, $query->posts);

        $this->go_to(get_term_link($this->relatedTag));
        $query = (new QueryHelper(true))->related('post', 'post_tag');
        $this->assertCount(0, $query->posts);
    }

    /** @test */
    public function relatedTo_can_spedify_which_post_to_be_related_by()
    {
        $serviceId = $this->relatedServiceIds[0];
        $service = get_post($this->relatedServiceIds[0]);

        $query = (new QueryHelper)
                    ->related('post', 'post_tag')
                    ->relatedTo($serviceId);

        $query2 = (new QueryHelper)
                    ->related('post', 'post_tag')
                    ->relatedTo($service);

        $query3 = (new QueryHelper)
                    ->related('post', 'post_tag', $serviceId);

        $query4 = (new QueryHelper)
                    ->related('post', 'post_tag', $service);

        $this->assertCount(5, $query->posts);
        $this->assertCount(5, $query2->posts);
        $this->assertCount(5, $query3->posts);
        $this->assertCount(5, $query4->posts);
    }

    /** @test */
    public function current_post_should_not_be_returned_when_relating_same_type()
    {
        $postId = $this->relatedPostIds[0];

        $query = (new QueryHelper)->related('post', 'post_tag', $postId);

        $this->assertCount(4, $query->posts);
        $this->assertFalse(in_array($postId, wp_list_pluck($query->posts, 'ID')));
    }

    /** @test */
    public function post_without_tag_should_return_empty_results()
    {
        $post = $this->newPost();

        $query = (new QueryHelper)->related('post', 'post_tag', $post);

        $this->assertCount(0, $query->posts);
    }

    /** @test */
    public function post_with_multiple_related_terms_will_works()
    {
        $serviceId = $this->relatedServiceIds[0];
        $term = $this->newTerm('post_tag');
        $post = $this->newPost();
        $post2 = $this->newPost();
        wp_set_object_terms($serviceId, [$term->term_id], 'post_tag', true);
        wp_set_object_terms($post->ID, [$term->term_id], 'post_tag', true);
        wp_set_object_terms($post2->ID, [$term->term_id], 'post_tag', true);

        $query = (new QueryHelper)->related('post', 'post_tag', $serviceId);

        $this->assertCount(7, $query->posts);
        $this->assertTrue(in_array($post->ID, wp_list_pluck($query->posts, 'ID')));
        $this->assertTrue(in_array($post2->ID, wp_list_pluck($query->posts, 'ID')));
    }

    /** @test */
    public function it_can_set_excluded_term()
    {
        $serviceId = $this->relatedServiceIds[0];
        $term = $this->newTerm('post_tag');
        $post = $this->newPost();
        wp_set_object_terms($serviceId, [$term->term_id], 'post_tag', true);
        wp_set_object_terms($post->ID, [$term->term_id], 'post_tag', true);

        $query = (new QueryHelper)
                    ->related('post', 'post_tag', $serviceId)
                    ->excludeTerms($term->term_id);

        $query2 = (new QueryHelper)
                    ->related('post', 'post_tag', $serviceId)
                    ->excludeTerms($term);

        $this->assertCount(5, $query->posts);
        $this->assertFalse(in_array($post->ID, wp_list_pluck($query->posts, 'ID')));

        $this->assertCount(5, $query2->posts);
        $this->assertFalse(in_array($post->ID, wp_list_pluck($query2->posts, 'ID')));
    }

    /** @test */
    public function it_can_set_excluded_terms()
    {
        $serviceId = $this->relatedServiceIds[0];
        $term = $this->newTerm('post_tag');
        $term2 = $this->newTerm('post_tag');
        $post = $this->newPost();
        $post2 = $this->newPost();
        wp_set_object_terms($serviceId, [$term->term_id], 'post_tag', true);
        wp_set_object_terms($serviceId, [$term2->term_id], 'post_tag', true);
        wp_set_object_terms($post->ID, [$term->term_id], 'post_tag', true);
        wp_set_object_terms($post2->ID, [$term2->term_id], 'post_tag', true);

        $query = (new QueryHelper)
                    ->related('post', 'post_tag', $serviceId)
                    ->excludeTerms([$term, $term2]);

        $this->assertCount(5, $query->posts);
        $this->assertFalse(in_array($post->ID, wp_list_pluck($query->posts, 'ID')));
        $this->assertFalse(in_array($post2->ID, wp_list_pluck($query->posts, 'ID')));
    }

    /** @test */
    public function it_can_set_excluded_post()
    {
        $serviceId = $this->relatedServiceIds[0];
        $postId = $this->relatedPostIds[2];

        $query = (new QueryHelper)
                    ->related('post', 'post_tag', $serviceId)
                    ->excludePosts($postId);

        $query2 = (new QueryHelper)
                    ->related('post', 'post_tag', $serviceId)
                    ->excludePosts(get_post($postId));

        $this->assertCount(4, $query->posts);
        $this->assertFalse(in_array($postId, wp_list_pluck($query->posts, 'ID')));

        $this->assertCount(4, $query2->posts);
        $this->assertFalse(in_array($postId, wp_list_pluck($query2->posts, 'ID')));
    }

    /** @test */
    public function it_can_set_excluded_posts()
    {
        $serviceId = $this->relatedServiceIds[0];
        $postId = $this->relatedPostIds[2];
        $postId2 = $this->relatedPostIds[3];

        $query = (new QueryHelper)
                    ->related('post', 'post_tag', $serviceId)
                    ->excludePosts([$postId, $postId2]);

        $this->assertCount(3, $query->posts);
        $this->assertFalse(in_array($postId, wp_list_pluck($query->posts, 'ID')));
        $this->assertFalse(in_array($postId2, wp_list_pluck($query->posts, 'ID')));
    }
}
