<?php

use Carbon\Carbon;
use Gummiforweb\WpHelpers\WpQuery\QueryHelper;

class TaxonomyTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->registerPostType('service');
        $this->registerTaxonomy('service_type', 'service');
        $this->createPosts(15, ['post_type' => 'service']);
    }

    /** @test */
    public function taxonomy_will_filter_post_in_that_term()
    {
        $term = $this->newTerm('service_type');
        $this->creaatePostsWithTerm(8, 'service', $term);

        $query = (new QueryHelper)->query('service')->taxonomy($term);
        $query2 = (new QueryHelper)->query('service')->taxonomy($term->term_id);
        $query3 = (new QueryHelper)->query('service')->taxonomy($term->slug, 'service_type');

        $this->assertCount(8, $query->posts);
        $this->assertCount(8, $query2->posts);
        $this->assertCount(8, $query3->posts);
    }

    /** @test */
    public function taxonomy_will_take_multiple_terms_and_use_AND_operator()
    {
        $term = $this->newTerm('service_type');
        $term2 = $this->newTerm('service_type');
        $postIds = $this->creaatePostsWithTerm(8, 'service', $term);
        foreach (array_slice($postIds, 0, 3) as $postId) {
            wp_set_post_terms($postId, [$term2->term_id], 'service_type', true);
        }

        $query = (new QueryHelper)->query('service')->taxonomy([$term, $term2]);
        $query2 = (new QueryHelper)->query('service')->taxonomy([$term->term_id, $term2->term_id]);
        $query3 = (new QueryHelper)->query('service')->taxonomy([$term->slug, $term2->slug], 'service_type');

        $this->assertCount(3, $query->posts);
        $this->assertCount(3, $query2->posts);
        $this->assertCount(3, $query3->posts);
    }

    /** @test */
    public function taxonomyIn_will_filter_posts_containe_either_term()
    {
        $term = $this->newTerm('service_type');
        $term2 = $this->newTerm('service_type');
        $this->creaatePostsWithTerm(3, 'service', $term);
        $this->creaatePostsWithTerm(4, 'service', $term2);

        $query = (new QueryHelper)->query('service')->taxonomyIn([$term, $term2]);
        $query2 = (new QueryHelper)->query('service')->taxonomyIn([$term->term_id, $term2->term_id]);
        $query3 = (new QueryHelper)->query('service')->taxonomyIn([$term->slug, $term2->slug], 'service_type');

        $this->assertCount(7, $query->posts);
        $this->assertCount(7, $query2->posts);
        $this->assertCount(7, $query3->posts);
    }

    /** @test */
    public function taxonomy_and_taxonomyIn_can_be_mixed()
    {
        $term = $this->newTerm('service_type');
        $term2 = $this->newTerm('service_type');
        $term3 = $this->newTerm('service_type');
        $postIds = $this->creaatePostsWithTerm(10, 'service', $term);
        $this->creaatePostsWithTerm(3, 'service', $term2);
        $this->creaatePostsWithTerm(4, 'service', $term3);
        foreach (array_slice($postIds, 0, 3) as $postId) {
            wp_set_post_terms($postId, [$term2->term_id], 'service_type', true);
        }
        foreach (array_slice($postIds, 5, 2) as $postId) {
            wp_set_post_terms($postId, [$term3->term_id], 'service_type', true);
        }

        $query = (new QueryHelper)->query('service')
                    ->taxonomy($term)
                    ->taxonomyIn([$term2, $term3]);

        $query2 = (new QueryHelper)->query('service')
                    ->taxonomy($term->term_id)
                    ->taxonomyIn([$term2->term_id, $term3->term_id]);

        $query3 = (new QueryHelper)->query('service')
                    ->taxonomy($term->slug, 'service_type')
                    ->taxonomyIn([$term2->slug, $term3->slug], 'service_type');

        $this->assertCount(5, $query->posts);
        $this->assertCount(5, $query2->posts);
        $this->assertCount(5, $query3->posts);
    }
}
