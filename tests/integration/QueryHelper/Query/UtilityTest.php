<?php

use Gummiforweb\WpHelpers\WpQuery\QueryHelper;

class UtilityTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        QueryHelper::clearCache();
        QueryHelper::clearTemplateLoop();
        $this->createPosts(20);
    }

    /** @test */
    public function it_will_return_query_post_counts()
    {
        $query = new QueryHelper;

        $this->assertEquals(10, $query->count());
    }

    /** @test */
    public function it_will_return_total_posts_found()
    {
        $query = new QueryHelper;

        $this->assertEquals(20, $query->total());
    }

    /** @test */
    public function it_will_return_if_current_query_is_empty()
    {
        $query = (new QueryHelper)->searchText('ABCDE');

        $this->assertCount(0, $query->posts);
        $this->assertTrue($query->isEmpty());
    }

    /** @test */
    public function it_will_return_the_current_loop_index_in_while_loop()
    {
        $query = (new QueryHelper)->take(5);

        $index = 0;
        while ($query->have_posts()): $query->the_post();
            $this->assertEquals($query->current_post, $query->currentIndex());
            $this->assertEquals($index, $query->currentIndex());
            $index ++;
        endwhile;
    }

    /** @test */
    public function it_will_return_the_current_loop_index_in_template_loop()
    {
        add_action('get_template_part_some-file', function() {
            printf(
                '<div data-index="%d"></div>',
                (new QueryHelper)->last()->currentIndex()
            );
        });

        ob_start();
        $query = (new QueryHelper)->cache()->take(5)->loop('some-file');
        $crawler = $this->crawler(ob_get_clean());

        $this->assertEquals(0, $crawler->filter('div:first-child')->attr('data-index'));
        $this->assertEquals(4, $crawler->filter('div:last-child')->attr('data-index'));
    }

    /** @test */
    public function it_will_return_if_current_post_is_odd_or_even()
    {
        $query = (new QueryHelper)->take(5);

        $count = 0;
        while ($query->have_posts()): $query->the_post();
            $count ++;
            $this->assertEquals($count % 2 == 1, $query->isOdd());
            $this->assertEquals($count % 2 == 0, $query->isEven());
        endwhile;
    }

    /** @test */
    public function it_will_indicate_current_index_of_numbers_per_row()
    {
        $query = (new QueryHelper)->take(10);

        $count = 0;
        while ($query->have_posts()): $query->the_post();
            $this->assertEquals(($count % 3 + 1), $query->currentRowIndex(3));
            $count ++;
        endwhile;

        $count = 0;
        while ($query->have_posts()): $query->the_post();
            $this->assertEquals(($count % 4 + 1), $query->currentRowIndex(4));
            $count ++;
        endwhile;
    }
}
