<?php

use Gummiforweb\WpHelpers\WpQuery\QueryHelper;

class QueryHelperTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        QueryHelper::clearCache();
    }

    /** @test */
    public function it_should_be_able_to_act_as_normal_wp_query()
    {
        $query = (new QueryHelper)->query();

        $this->assertTrue(is_callable([$query, 'have_posts']));
        $this->assertTrue(is_callable([$query, 'the_post']));
    }

    /** @test */
    public function it_will_return_current_wp_query()
    {
        $page = $this->factory->post->create(['post_type' => 'page']);
        $this->go_to(get_permalink($page));

        $query = new QueryHelper(true);
        $query2 = (new QueryHelper)->getGlobal();

        $this->assertEquals($query->post, $GLOBALS['wp_query']->post);
        $this->assertEquals($query2->post, $GLOBALS['wp_query']->post);
    }

    /** @test */
    public function it_will_return_current_query_on_taxonomy_page()
    {
        $term = $this->factory->term->create();
        $this->go_to(get_term_link($term));

        $query = new QueryHelper(true);
        $query2 = (new QueryHelper)->getGlobal();

        $this->assertTrue($query->is_tag());
        $this->assertEquals($query->query_vars, $GLOBALS['wp_query']->query_vars);
        $this->assertTrue($query2->is_tag());
        $this->assertEquals($query2->query_vars, $GLOBALS['wp_query']->query_vars);
    }

    /** @test */
    public function it_can_be_iterate_through_foreach()
    {
        $posts = $this->createPosts(20);

        $query = (new QueryHelper)->query()->take(10);

        foreach ($query as $i => $post) {
            $this->assertEquals($posts[19 - $i], $post->ID);
        }
    }

    /** @test */
    // public function it_will_clear_wp_query_after_foreach()
    // {
    //     setup_postdata($GLOBALS['post'] = $page = $this->factory->post->create_and_get(['post_type' => 'page']));

    //     $posts = $this->createPosts(20);
    //     $query = (new QueryHelper)->query()->take(10);

    //     $this->assertEquals($page->ID, get_the_ID());
    //     foreach ($query as $i => $post) {}
    //     $this->assertEquals($page->ID, get_the_ID());
    // }

    /** @test */
    public function it_passing_nothing_is_equivalent_to_querying_posts()
    {
        $posts = $this->createPosts(20);
        $this->createPosts(20, ['post_type' => 'page']);

        $query = new QueryHelper;

        $this->assertEquals(
            array_slice($posts, 10, 10),
            array_reverse(wp_list_pluck($query->posts, 'ID'))
        );
    }

    /** @test */
    public function it_can_retreive_the_last_query_builder()
    {
        $query = (new QueryHelper)->cache();
        $query2 = (new QueryHelper)->last();
        $query3 = new QueryHelper;

        $this->assertSame($query, $query2);
        $this->assertNotSame($query, $query3);
    }

    /** @test */
    public function it_will_return_current_query_if_no_latest_query_exists()
    {
        $query = (new QueryHelper);
        $query2 = (new QueryHelper)->last();
        $query3 = new QueryHelper(true);

        $this->assertNotSame($query, $query2);
        $this->assertNotNull($query2);
        $this->assertSame($query2->getWpQuery(), $query3->getWpQuery());
    }

    /** @test */
    public function cache_can_be_cleared()
    {
        $this->createPosts(5);
        $query = (new QueryHelper)->cache()->take(5);

        $this->assertSame($query, (new QUeryHelper)->last());

        QueryHelper::clearCache();

        $this->assertNotSame($query, (new QUeryHelper)->last());
    }
}
