<?php

use Gummiforweb\WpHelpers\WpQuery\QueryHelper;

class PaginationTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->createPosts(50);
    }

    /** @test */
    public function it_can_get_current_page()
    {
        $query = (new QueryHelper);
        $this->assertEquals(1, $query->currentPage());

        set_query_var('paged', 2);
        $query2 = (new QueryHelper);
        $this->assertEquals(2, $query->currentPage());
    }

    /** @test */
    public function it_can_get_total_page()
    {
        $query = (new QueryHelper);
        $this->assertEquals(5, $query->totalPage());
    }

    /** @test */
    public function it_can_get_current_page_on_archive()
    {
        $this->go_to(add_query_arg(['paged' => 2], get_term_link(1)));

        $query = new QueryHelper(true);

        $this->assertEquals(2, $query->currentPage());
    }

    /** @test */
    public function it_will_return_empty_previous_page_url_or_link_on_first_page()
    {
        $query = (new QueryHelper);

        $this->assertEquals('', $query->prevPage());
        $this->assertEquals('', $query->prevPageUrl());
    }

    /** @test */
    public function it_will_output_previous_page_url_or_link_if_not_on_first_page()
    {
        set_query_var('paged', 3);
        $query = (new QueryHelper);

        $this->assertRegExp('#<a href=".*paged=2.*".*>Previous Page</a>#', $query->prevPage());
        $this->assertRegExp('#paged=2#', $query->prevPageUrl());
    }

    /** @test */
    public function it_will_return_empty_next_page_url_or_link_on_last_page()
    {
        set_query_var('paged', 5);
        $query = (new QueryHelper);

        $this->assertEquals('', $query->nextPage());
        $this->assertEquals('', $query->nextPageUrl());
    }

    /** @test */
    public function it_will_output_next_page_url_or_link_if_not_on_last_page()
    {
        set_query_var('paged', 3);
        $query = (new QueryHelper);

        $this->assertRegExp('#<a href=".*paged=4.*".*>Next Page</a>#', $query->nextPage());
        $this->assertRegExp('#paged=4#', $query->nextPageUrl());
    }

    /** @test */
    public function prePage_and_nextPage_text_label_can_be_custom()
    {
        set_query_var('paged', 3);
        $query = (new QueryHelper);

        $this->assertRegExp('#<a.*>Prev</a>#', $query->prevPage('Prev'));
        $this->assertRegExp('#<a.*>Next</a>#', $query->nextPage('Next'));
    }

    /** @test */
    public function it_will_populate_page_numbers_list()
    {
        $query = (new QueryHelper);
        $crawler = $this->crawler($query->pageNumbers());

        $this->assertCount(1, $crawler->filter('ul'));
        $this->assertCount(5, $crawler->filter('li'));
        $this->assertCount(4, $crawler->filter('a'));
        $this->assertCount(1, $crawler->filter('span'));
    }

    /** @test */
    public function pagination_list_class_can_be_added()
    {
        $query = (new QueryHelper);
        $crawler = $this->crawler($query->pageNumbers('my-custom-class'));

        $this->assertCount(1, $crawler->filter('.pagination-list'));
        $this->assertCount(1, $crawler->filter('.my-custom-class'));
    }

    /** @test */
    public function pagination_dots_should_be_in_the_right_spot()
    {
        $this->createPosts(100); // total of 150 posts, 15 pages

        $crawlerPage1 = $this->crawler($data=(new QueryHelper)->pageNumbers());

        $this->assertCount(1, $crawlerPage1->filter('.pagination-dots'));

        set_query_var('paged', 2);
        $crawlerPage2 = $this->crawler((new QueryHelper)->pageNumbers());

        $this->assertCount(1, $crawlerPage2->filter('.pagination-dots'));

        set_query_var('paged', 6);
        $crawlerPage6 = $this->crawler((new QueryHelper)->pageNumbers());

        $this->assertCount(2, $crawlerPage6->filter('.pagination-dots'));
    }

    /** @test */
    public function it_will_not_return_page_numbers_if_only_one_page()
    {
        $query = (new QueryHelper)->take(50);
        $crawler = $this->crawler($query->pageNumbers());

        $this->assertEquals('', $query->pageNumbers());
        $this->assertCount(0, $crawler->filter('body > *'));
    }

    /** @test */
    public function pagination_will_include_prev_next_in_pagination_list()
    {
        set_query_var('paged', 2);
        $query = (new QueryHelper);
        $crawler = $this->crawler($data = $query->pagination());

        $this->assertCount(1, $crawler->filter('ul'));
        $this->assertCount(7, $crawler->filter('li'));
        $this->assertCount(6, $crawler->filter('a'));
        $this->assertCount(1, $crawler->filter('span'));
    }

    /** @test */
    public function it_will_not_return_pagination_if_only_one_page()
    {
        $query = (new QueryHelper)->take(50);
        $crawler = $this->crawler($query->pagination());

        $this->assertEquals('', $query->pagination());
        $this->assertCount(0, $crawler->filter('body > *'));
    }

    /** @test */
    public function pagination_prev_label_and_next_label_can_be_changed()
    {
        set_query_var('paged', 2);
        $query = (new QueryHelper);
        $crawler = $this->crawler($data = $query->pagination('', '-P', 'N-'));

        $this->assertEquals('-P', $crawler->filter('li:first-child a')->text());
        $this->assertEquals('N-', $crawler->filter('li:last-child a')->text());
    }

    /** @test */
    public function it_should_include_current_query_url_args()
    {
        $this->go_to(add_query_arg(['abc' => 123]));

        set_query_var('paged', 3);
        $query = (new QueryHelper);
        $crawler = $this->crawler($query->pagination());

        $crawler->filter('a')->each(function($node, $i) {
            $this->assertRegExp('#abc=123#', $node->attr('href'));
        });
        $this->assertRegExp('#abc=123#', $query->nextPageUrl());
        $this->assertRegExp('#abc=123#', $query->nextPageUrl());
    }

    /** @test */
    public function pagination_can_accept_custom_arguments()
    {
        set_query_var('paged', 3);
        $query = (new QueryHelper);
        $crawler = $this->crawler($query->pagination('', 'prev', 'next', ['base' => 'http://testtest.com']));

        $crawler->filter('a')->each(function($node, $i) {
            $this->assertRegExp('#http://testtest.com#', $node->attr('href'));
        });
    }

    /** @test */
    public function pagination_prev_next_li_will_have_proper_class()
    {
        set_query_var('paged', 2);
        $query = (new QueryHelper);
        $crawler = $this->crawler($query->pagination());

        $crawler->filter('li:first-child')->each(function($node, $i) {
            $this->assertRegexp('/pagination-prev-item/', $node->attr('class'));
        });

        $crawler->filter('li:last-child')->each(function($node, $i) {
            $this->assertRegexp('/pagination-next-item/', $node->attr('class'));
        });
    }

    /** @test */
    public function pagination_prev_next_link_should_not_have_pagination_number_class()
    {
        set_query_var('paged', 2);
        $query = (new QueryHelper);
        $crawler = $this->crawler($query->pagination());

        $crawler->filter('li:first-child a')->each(function($node, $i) {
            $this->assertNotRegexp('/pagination-number/', $node->attr('class'));
        });

        $crawler->filter('li:last-child a')->each(function($node, $i) {
            $this->assertNotRegexp('/pagination-number/', $node->attr('class'));
        });
    }
}
