<?php

use Gummiforweb\WpHelpers\WpQuery\QueryHelper;

class SearchComponentsTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->termIds = $this->factory->term->create_many(10, ['taxonomy' => 'post_tag']);
        wp_set_post_terms($this->newPost()->ID, $this->termIds, 'post_tag', true);
    }

    /** @test */
    public function it_will_output_select_with_correct_selection_selected()
    {
        $term = get_term($this->termIds[3]);
        $_GET['post_tag'] = $term->slug;

        $options = (new QueryHelper)->searchTaxonomyOptions('post_tag');
        $crawler = $this->crawler($options);

        $this->assertCount(10, $crawler->filter('option'));
        $this->assertCount(1, $crawler->filter('option[selected]'));
        $this->assertEquals($term->name, $crawler->filter('option[selected]')->first()->text());
    }

    /** @test */
    public function it_will_output_select_with_correct_selection_on_archive_page()
    {
        $term = get_term($this->termIds[3]);
        $this->go_to(get_term_link($term));

        $options = (new QueryHelper)->searchTaxonomyOptions('post_tag');
        $crawler = $this->crawler($options);

        $this->assertCount(10, $crawler->filter('option'));
        $this->assertCount(1, $crawler->filter('option[selected]'));
        $this->assertEquals($term->name, $crawler->filter('option[selected]')->first()->text());
    }

    /** @test */
    public function it_can_overwrite_which_url_key_match_with_the_taxonomy_for_options()
    {
        $term = get_term($this->termIds[3]);
        $_GET['my-tag'] = $term->slug;

        $options = (new QueryHelper)->searchTaxonomyOptions('post_tag', 'my-tag');
        $crawler = $this->crawler($options);

        $this->assertCount(10, $crawler->filter('option'));
        $this->assertCount(1, $crawler->filter('option[selected]'));
        $this->assertEquals($term->name, $crawler->filter('option[selected]')->first()->text());
    }

    /** @test */
    public function it_will_output_lists_with_correct_current_class()
    {
        $term = get_term($this->termIds[3]);
        $_GET['post_tag'] = $term->slug;

        $lists = (new QueryHelper)->searchTaxonomyLists('post_tag');
        $crawler = $this->crawler($lists);

        $this->assertCount(10, $crawler->filter('li'));
        $this->assertCount(1, $crawler->filter('li.current'));
        $this->assertEquals($term->name, $crawler->filter('li.current')->first()->text());
    }

    /** @test */
    public function it_will_output_lists_with_correct_current_class_on_archive_page()
    {
        $term = get_term($this->termIds[3]);
        $this->go_to(get_term_link($term));

        $lists = (new QueryHelper)->searchTaxonomyLists('post_tag');
        $crawler = $this->crawler($lists);

        $this->assertCount(10, $crawler->filter('li'));
        $this->assertCount(1, $crawler->filter('li.current'));
        $this->assertEquals($term->name, $crawler->filter('li.current')->first()->text());
    }

    /** @test */
    public function it_can_overwrite_which_url_key_match_with_the_taxonomy_for_lists()
    {
        $term = get_term($this->termIds[3]);
        $_GET['my-tag'] = $term->slug;

        $lists = (new QueryHelper)->searchTaxonomyLists('post_tag', 'my-tag');
        $crawler = $this->crawler($lists);

        $this->assertCount(10, $crawler->filter('li'));
        $this->assertCount(1, $crawler->filter('li.current'));
        $this->assertEquals($term->name, $crawler->filter('li.current')->first()->text());
    }

    /** @test */
    public function it_can_overwrite_the_current_class_name_for_lists()
    {
        $term = get_term($this->termIds[3]);
        $_GET['my-tag'] = $term->slug;

        $lists = (new QueryHelper)->searchTaxonomyLists('post_tag', 'my-tag', 'active');
        $crawler = $this->crawler($lists);

        $this->assertCount(10, $crawler->filter('li'));
        $this->assertCount(0, $crawler->filter('li.current'));
        $this->assertCount(1, $crawler->filter('li.active'));
        $this->assertEquals($term->name, $crawler->filter('li.active')->first()->text());
    }

    /** @test */
    public function it_will_output_the_search_keywords_input()
    {
        $input = (new QueryHelper)->searchInput();
        $crawler = $this->crawler($input);

        $this->assertCount(1, $crawler->filter('input'));
        $this->assertCount(1, $crawler->filter('input[name="search"]'));
    }

    /** @test */
    public function it_will_output_the_search_keywords_input_with_value()
    {
        $_GET['search'] = 'walala';

        $input = (new QueryHelper)->searchInput();
        $crawler = $this->crawler($input);

        $this->assertCount(1, $crawler->filter('input'));
        $this->assertCount(1, $crawler->filter('input[value="walala"]'));
    }

    /** @test */
    public function query_key_for_search_input_can_be_changed()
    {
        $_GET['search'] = 'balala';
        $_GET['keywords'] = 'walala';

        $input = (new QueryHelper)->searchInput('keywords');
        $crawler = $this->crawler($input);

        $this->assertCount(1, $crawler->filter('input'));
        $this->assertCount(1, $crawler->filter('input[name="keywords"]'));
        $this->assertCount(1, $crawler->filter('input[value="walala"]'));
    }

    /** @test */
    public function it_will_allow_custom_class_on_search_input()
    {
        $input = (new QueryHelper)->searchInput('search', 'my-searches');
        $crawler = $this->crawler($input);

        $this->assertCount(1, $crawler->filter('input'));
        $this->assertCount(1, $crawler->filter('input.my-searches'));
    }

    /** @test */
    public function it_will_allow_custom_placeholder_text_on_search_input()
    {
        $input = (new QueryHelper)->searchInput('search', 'my-searches', 'Enter your name...');
        $crawler = $this->crawler($input);

        $this->assertCount(1, $crawler->filter('input'));
        $this->assertCount(1, $crawler->filter('input[placeholder="Enter your name..."]'));
    }
}
