<?php

use Gummiforweb\WpHelpers\WpQuery\QueryHelper;

class TemplateTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        QueryHelper::clearTemplateLoop();
        $this->postIds = $this->createPosts(20);

        add_action('get_template_part_templates/loop/post', function() {
            printf('<div id="post-%d">%s</div>', get_the_ID(), get_the_title());
        });
    }

    /** @test */
    public function it_can_loop_the_results_with_template_part()
    {
        ob_start();
        $query = (new QueryHelper)->loop('templates/loop/post');
        $crawler = $this->crawler(ob_get_clean());

        $this->assertCount(10, $crawler->filter('div'));
    }

    /** @test */
    public function it_can_continue_to_do_something_else_after_loop()
    {
        ob_start();
        $query = (new QueryHelper)->loop('templates/loop/post');
        ob_get_clean();

        $this->assertInstanceOf(QueryHelper::class, $query);
    }

    /** @test */
    public function it_can_use_dots_as_slash_for_path()
    {
        ob_start();
        $query = (new QueryHelper)->loop('templates.loop.post');
        $crawler = $this->crawler(ob_get_clean());

        $this->assertCount(10, $crawler->filter('div'));
    }

    /** @test */
    public function it_should_strip_away_extension_from_path()
    {
        ob_start();
        $query = (new QueryHelper)->loop('templates/loop/post.php');
        $crawler = $this->crawler(ob_get_clean());

        $this->assertCount(10, $crawler->filter('div'));
    }

    /** @test */
    public function it_can_set_default_loop_start_and_end()
    {
        QueryHelper::setTemplateLoop([
            'before_loop' => '<ul>', 'after_loop' => '</ul>',
            'before_item' => '<li>', 'after_item' => '</li>',
        ]);

        ob_start();
        $query = (new QueryHelper)->loop('templates/loop/post');
        $crawler = $this->crawler(ob_get_clean());

        $this->assertCount(1, $crawler->filter('ul'));
        $this->assertCount(10, $crawler->filter('li'));
        $this->assertCount(10, $crawler->filter('div'));
    }

    /** @test */
    public function it_can_pass_run_time_argument()
    {
        QueryHelper::setTemplateLoop([
            'before_loop' => '<ul>', 'after_loop' => '</ul>',
            'before_item' => '<li>', 'after_item' => '</li>',
        ]);

        ob_start();
        $query = (new QueryHelper)->loop('templates/loop/post', [
            'before_loop' => '<ul class="ha">',
            'before_item' => '<li class="meh">',
        ]);
        $crawler = $this->crawler(ob_get_clean());

        $this->assertCount(1, $crawler->filter('.ha'));
        $this->assertCount(10, $crawler->filter('.meh'));
        $this->assertCount(10, $crawler->filter('div'));
    }

    /** @test */
    public function it_should_set_before_loop_if_second_argument_is_string()
    {
        QueryHelper::setTemplateLoop([
            'before_loop' => '<ul>', 'after_loop' => '</ul>',
            'before_item' => '<li>', 'after_item' => '</li>',
        ]);

        ob_start();
        $query = (new QueryHelper)->loop('templates/loop/post', '<ul class="ha">');
        $crawler = $this->crawler(ob_get_clean());

        $this->assertCount(1, $crawler->filter('.ha'));
        $this->assertCount(10, $crawler->filter('div'));
    }

    /** @test */
    public function it_will_ignore_output_args_if_second_argument_is_false()
    {
        QueryHelper::setTemplateLoop([
            'before_loop' => '<ul>', 'after_loop' => '</ul>',
            'before_item' => '<li>', 'after_item' => '</li>',
        ]);

        ob_start();
        $query = (new QueryHelper)->loop('templates/loop/post', false);
        $crawler = $this->crawler(ob_get_clean());

        $this->assertCount(0, $crawler->filter('ul'));
        $this->assertCount(0, $crawler->filter('li'));
        $this->assertCount(10, $crawler->filter('div'));
    }

    /** @test */
    public function it_can_reset_output_args_to_all_empty()
    {
        QueryHelper::setTemplateLoop([
            'before_loop' => '<ul>', 'after_loop' => '</ul>',
            'before_item' => '<li>', 'after_item' => '</li>',
        ]);

        ob_start();
        $query = (new QueryHelper)->loop('templates/loop/post');
        $output = ob_get_clean();

        ob_start();
        $query = (new QueryHelper)->loop('templates/loop/post');
        $output2 = ob_get_clean();

        QueryHelper::clearTemplateLoop();

        ob_start();
        $query = (new QueryHelper)->loop('templates/loop/post');
        $output3 = ob_get_clean();

        $this->assertEquals($output, $output2);
        $this->assertNotEquals($output, $output3);
    }
}
