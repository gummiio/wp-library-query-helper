<?php
$pluginDir = dirname(dirname(__FILE__));
$wpRoot    = dirname(dirname(dirname(dirname($pluginDir)))); // \src\wp-content\plugins\$pluginDir
$testRoot  = "$wpRoot/tests/phpunit";

require_once $testRoot . '/includes/functions.php';

tests_add_filter('muplugins_loaded', function() {
    update_option('active_plugins', [
        'wp-query-helper/index.php',
    ]);
});

require_once $testRoot . '/includes/bootstrap.php';
