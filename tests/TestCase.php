<?php

use Carbon\Carbon;
use Symfony\Component\DomCrawler\Crawler;

class TestCase extends WP_UnitTestCase
{
    protected function assertOutput($assertion, $closure, $trim = false)
    {
        ob_start();

        $closure();
        $output = ob_get_clean();

        $this->assertEquals($assertion, $trim? trim($output) : $output);
    }

    protected function assertOutputRegExp($assertion, $closure, $trim = false)
    {
        ob_start();

        $closure();
        $output = ob_get_clean();

        $this->assertRegExp($assertion, $trim? trim($output) : $output);
    }

    protected function crawler($output, $addBody = true)
    {
        if ($addBody) {
            $output = "<body>{$output}</body>";
        }

        return new Crawler($output);
    }

    protected function registerPostType($posttype, $args = [])
    {
        register_post_type($posttype, wp_parse_args($args, [
            'label'  => ucfirst($posttype),
            'public' => true
        ]));
    }

    protected function registerTaxonomy($taxonomy, $posttype, $args = [])
    {
        register_taxonomy($taxonomy, $posttype, wp_parse_args($args, [
            'label'  => ucfirst($taxonomy)
        ]));
    }

    protected function newPost($postType = 'post', $args = [])
    {
        return $this->factory->post->create_and_get(wp_parse_args([
            'post_type' => $postType
        ], $args));
    }

    protected function newTerm($taxonomy = 'category', $args = [])
    {
        return $this->factory->term->create_and_get(wp_parse_args([
            'taxonomy' => $taxonomy
        ], $args));
    }

    protected function createPosts($count, $args = [], $startDate = null)
    {
        $posts = [];
        $time = $startDate? : (new Carbon)->subDay(); // incase of "future" post

        for ($i = 0; $i < $count; $i ++) {
            $posts[] = $this->factory->post->create(wp_parse_args([
                'post_date' => $time->addSecond($i)->format('Y-m-d H:i:s')
            ], $args));
        }

        return $posts;
    }

    protected function creaatePostsWithTerm($count, $posttype, $term)
    {
        $term = get_term($term);

        $posts = $this->createPosts($count, ['post_type' => $posttype]);

        foreach ($posts as $post) {
            wp_set_post_terms($post, [$term->term_id], $term->taxonomy);
        }

        return $posts;
    }

    protected function creaatePostTypeWithOrder($posttype, $orderby, $orders, $startDate = null)
    {
        $posts = [];
        $time = $startDate? : (new Carbon)->subDay(); // incase of "future" post

        foreach ($orders as $i => $order) {
            $posts[] = $this->factory->post->create([
                'post_type' => $posttype,
                'post_date' => $time->addSecond($i)->format('Y-m-d H:i:s'),
                $orderby => $order
            ]);
        }

        return $posts;
    }

    protected function creaatePostTypeWithTermOrder($posttype, $term, $orders, $startDate = null)
    {
        $posts = [];
        $time = $startDate? : (new Carbon)->subDay(); // incase of "future" post

        foreach ($orders as $i => $order) {
            $postId = $this->factory->post->create([
                'post_type' => $posttype,
                'post_date' => $time->addSecond($i)->format('Y-m-d H:i:s')
            ]);
            wp_set_post_terms($postId, [$term->term_id], $term->taxonomy);
            update_post_meta($postId, "_reorder_term_{$term->taxonomy}_{$term->slug}", $order);

            $posts[] = $postId;
        }

        return $posts;
    }
}
