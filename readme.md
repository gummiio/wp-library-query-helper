# WordPress Plugin - WP Query Helper

A more fluid and elegant way to write complex wp query.

[ ![Bitbucket Pipeline Status for gummiio/wp-library-query-helper](https://bitbucket-badges.atlassian.io/badge/gummiio/wp-library-query-helper.svg)](https://bitbucket.org/gummiio/wp-library-query-helper/overview)

# Example
```
<?php
    // 5 latest post
    query_helper('post', 5)->latest()

    // with taxonomy
    $term = get_term(5);
    query_helper('post', 5)->taxonomy($term)

    // with meta
    query_helper('post', 5)->meta('thumbnail_id', 5)

    // template loop
    query_helper('post', 5)->loop('templates/loop/post')

    // search by url query search?service_type=news
    query_helper('post', 5)->search()

    // related services by current post's post tags
    query_helper()->related('service', 'post_tag')->limit(5)

    // pagination
    query_helper()->pagination()

    // or retreive the last query helper in other template file
    last_query_helper()->pagination()

    ... and more
```
